====================
drm/i915 Style Guide
====================

Draft style guide for drm/i915 development, augmenting the Linux kernel coding
style document.

There is nothing particularly new here, and it's not the intent to fundamentally
change anything, but rather document the existing style and thus far unwritten
rules.

.. contents::


Build
=====

* Use ``CONFIG_DRM_I915_WERROR=y`` when building the driver. This avoids leaving
  in warnings, and ensures headers are self-contained.

* Ensure each commit builds. Use something like ``git rebase $tip --exec="make
  -j$(nproc)"``, where ``$tip`` equals to your ``drm-tip`` remote and branch.

* Run ``checkpatch.pl`` on each commit. Use ``dim checkpatch <commit-range>`` or
  equivalent. ``dim`` contains the relevant parameters, including ``--strict``
  as well as some ignores.

* Run ``sparse`` on each commit. Use ``dim sparse <commit-range>`` or
  equivalent. (Note that CI does not run ``sparse`` on each commit.)


Naming Conventions
==================

Namespacing
-----------

* As the C language does not support namespacing, we have to use scope and
  prefixes.

* Avoid using name prefixes in i915 that clearly violate namespaces in core
  headers or other drivers.

File Names
----------

* For display code, use ``intel_<feature>.[ch]``.

* If a file is platform specific, use ``<tla>_<feature>.[ch]``, where ``<tla>``
  is the acronym for the platform.

Variables and Parameters
------------------------

* ``struct drm_i915_private *`` in local variables, function parameters and
  structure members should be named ``i915``. Do not add ``dev_priv`` in new
  code. Convert ``dev_priv`` to ``i915`` when refactoring existing code. (There
  are still some implicit ``dev_priv`` uses in register definitions that may
  mandate its use.)

* Prefer passing and using ``intel_<foo>`` types over corresponding
  ``drm_<foo>``. Here, ``<foo>`` would be something like connector or encoder.
  Use the drm variant only where required, like callbacks. For example::

    static void bar(struct drm_foo *_foo)
    {
            struct intel_foo *foo = to_foo(_foo);
	    ...
    }

* Avoid adding ``struct drm_device *`` local variables.

Functions
---------

* Non-static functions in file ``intel_<feature>.[ch]`` should be prefixed
  ``intel_<feature>_``.

* If there are platform specific non-static functions, they may also be prefixed
  ``<tla>_<feature>_``.

* Assertion functions may be prefixed ``assert_<feature>_``.


Error and Debug Logging
=======================

* Always prefer ``struct drm_device *`` based logging.


Internal Interfaces
===================

* Data is not an interface. Prefer exposing functions, not variables.


Header Files
============

* Avoid including headers from headers. Use forward declarations instead.

* If it seems like you need to include (lots of) headers from headers,
  reconsider your interface.

* Avoid ``static inline`` functions in headers. They are better than macros, but
  if it's not critical, make it a proper interface instead. ``static inline``
  forces you to leak your abstractions, and increases header
  interdependencies. Exceptions are stubs for Kconfig and inlines that don't
  depend on any external headers.

* Avoid adding anything to ``i915_drv.h``. It's a bloated header, and should not
  be considered a catch-all for everything. Similarly for ``intel_display.h``.

* In most cases, there should be a ``.h`` corresponding to each ``.c`` file,
  declaring the functions and types.


.c Files
========

* Avoid using ``inline``. In most cases, just let the compiler decide. The use
  should be limited and justified. Using ``inline`` drops the compiler check for
  unused functions.

* Each ``.c`` file probably needs to ``#include`` its corresponding ``.h`` file
  to declare the global functions, to avoid ``sparse`` warnings.


File Boilerplate
================

* Use SPDX instead of full license

* Avoid adding Author lines. They get stale quickly, and promote a false sense
  of code ownership for things that are fundamentally shared.

* Group ``#include`` directives by ``<linux>``, ``<drm>``, local i915
  sub-directory, no directory. Within groups, sort them alphabetically.


Global and Static Variables
===========================

* Do not add global variables in the driver. Almost everything needs to be
  device (i.e. ``struct drm_device`` or ``struct drm_i915_private``) specific,
  not driver or module specific.

* Similarly for static variables. They are driver/module specific.

* Use ``i915-check data`` to ensure you're not adding anything.


General
=======

* Avoid ``likely()`` and ``unlikely()``. In most cases, just let the compiler
  decide. Their use should be limited and justified, not added in a whim. The
  more you add them, the more they are cargo-culted.
